import base64
from flask import Flask, render_template, Response, stream_with_context
import os
import time

app = Flask(__name__)

def stream_file(name):
    with open(name, 'r') as f:
        while True:
            content = f.read()
            if len(content) == 0:
                time.sleep(0.05)
                continue

            yield f"data: {base64.b64encode(content.encode()).decode()}\n\n"

@app.route('/stdout_stream')
def stdout_stream():
    return Response(stream_with_context(stream_file('stdout.txt')), content_type='text/event-stream')

@app.route('/stderr_stream')
def stderr_stream():
    return Response(stream_with_context(stream_file('stderr.txt')), content_type='text/event-stream')

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True, threaded=True, port=8123)
